FROM r-base:4.0.3

WORKDIR /app

RUN apt-get update && \
    apt-get install -y libpq-dev r-cran-plumber r-cran-dbi

RUN Rscript -e "install.packages(c('RPostgres'))"

COPY *.R /app/

CMD [ "Rscript", "/app/main.R" ]