IMAGE_HOST ?= local
IMAGE_REPO ?= fin-serv
IMAGE_TAG ?= local

IMAGE := ${IMAGE_HOST}/${IMAGE_REPO}:${IMAGE_TAG}

docker-build:
	@docker build -t ${IMAGE} .

docker-publish: docker-build
	@docker push ${IMAGE}

docker-up:
	@IMAGE=${IMAGE} docker-compose up --build