begin;

CREATE TABLE IF NOT EXISTS items
(
    id          serial PRIMARY KEY,
    name        varchar UNIQUE NOT NULL,
    value       int,
    created     timestamp default now(),
    updated     timestamp default now()
    );

end;
