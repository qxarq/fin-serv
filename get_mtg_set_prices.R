library(rvest)
library(glue)
library(stringr)
library(dplyr)
library(purrr)
library(googlesheets4)

#Get and Clean Set Data
set_data <- 'https://scryfall.com/sets' %>%
  read_html %>%
  html_table %>%
  .[[1]]

set_data$name_raw = set_data$Name
set_data$Name = str_match(set_data$Name, "(^.+)\\s")[, 2]
set_data$symbol = str_match(set_data$name_raw, "\\s(\\w+)$")[, 2]
set_data <- set_data %>% rename_with(tolower)

#Sets whose price history to fetch
#Test Case
sets <- c('atog', 'Limited Edition Alpha', 'Limited Edition Beta', 'Unlimited Edition', 'Revised Edition')

#Get Set Data from googlesheets
sheet_id <- '1xPe-9TSaVkhDawGIL_GFC0jXtG5ajqUfpeFAeVQTlhc'

#Get Reserved List Data
reserved_list_cards_raw <- read_sheet(sheet_id, sheet = 'reserved_list_read')
reserved_list_cards <- reserved_list_cards_raw %>%
  select(name, set, amount, price_point, date) %>%
  filter(!is.na(set))

#Get Collection Data
collection_cards_raw <- read_sheet(sheet_id, sheet = 'collection_read')
collection_cards <- collection_cards_raw %>%
  select(name, set, amount, price_point, date)

#Get Singles Data
singles_cards_raw <- read_sheet(sheet_id, sheet = 'singles_read')
singles_cards <- singles_cards_raw %>%
  select(name, set, amount, price_point, date)

#Get Set List
reserved_list_sets <- reserved_list_cards %>%
  distinct(across(set))
collection_sets <- collection_cards_raw %>%
  distinct(across(set))
singles_sets <- singles_cards_raw %>%
  distinct(across(set))

#Union of all set lists
all_sets <- union(reserved_list_sets,
                  collection_sets,
                  singles_sets) %>%
  distinct(across(set))

#Join on full set name
all_sets <- left_join(all_sets, set_data, by=c('set' = 'symbol')) %>%
  select(set, name)

unknown_sets <- all_sets %>% filter(is.na(name))
known_sets <- all_sets %>% filter(!is.na(name))

#Return price data for a single set
get_mtg_set_price <- function(set_name) {
  formatted_set_name <- set_name %>%
    str_replace(' ', '+')
  url <-
    glue('https://www.mtggoldfish.com/sets/{formatted_set_name}#paper')
  data <- url %>%
    read_html %>%
    html_table %>%
    .[1] %>%
    .[[1]]
  data$set_name = set_name
  print(glue('Got data for {set_name}!'))
  Sys.sleep(2)
  return(data)
}

#Get Price Data for all the sets
set_prices <- map_df(known_sets$name, possibly(get_mtg_set_price, otherwise = NULL))
set_prices <- left_join(set_prices, set_data, by=c('set_name' = 'name')) %>%
  select(-languages, -date, -cards, -name_raw)