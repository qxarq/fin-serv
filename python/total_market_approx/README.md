#Purpose
The purpose of 'total_market_approx.py' is to discover the right allocations of small, medium, and large cap market indexes to create the closest stock style diversification to a given asset. 

#How to use
Diversifications by asset were determined using the morningstar instant x-ray app which can be accessed after creating a free account (http://morningstar.com/instant-x-ray).

Values for the diversification tables are used by saving the values to a vector stored as a csv called "<ticker>_table.csv," where <ticker> is the name of the asasset in question. Values are stored from left to right, top to bottom on the table such that


LV LC LG
MV MC MG
SV SC SG

becomes:

LV
LC
LG
MV
MC
MG
SV
SC
SG

By editing the name of the tickers to be used for large, medium, small cap, and target in "total_market_approx.py" you can use any three assets to approximate the diversification of a target asset. by running "total_market_approx.py" it will define the following values and print a recommended portfolio allocation:

x, y, y_approx, resid, b, mse
