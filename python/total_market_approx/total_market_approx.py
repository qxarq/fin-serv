import numpy as np

large_cap = "FXAIX"
medium_cap = "FSMDX"
small_cap = "FSSNX"
target = "VTI"
file_directory = "/home/qxarq/code/fin-serv/python/total_market_approx/"
file_names = "_table.csv"

y = np.loadtxt(file_directory + target + file_names) 
x1 = np.loadtxt(file_directory + large_cap + file_names) 
x2 = np.loadtxt(file_directory + medium_cap + file_names) 
x3 = np.loadtxt(file_directory + small_cap + file_names) 

x = np.transpose(np.array([x1, x2, x3]))


b = np.dot(np.linalg.inv(np.dot(np.transpose(x), x)), np.dot(np.transpose(x), y))
y_approx = np.dot(b, np.transpose(x))
resid = y - y_approx
mse = np.mean(resid**2)

print('With a mean squared error of ', mse, ', we recommend the following allocation\n',
large_cap, ': ', "{:.2f}".format(b[0]*100), '%\n',
medium_cap, ': ', "{:.2f}".format(b[1]*100), '%\n',
small_cap, ': ', "{:.2f}".format(b[2]*100), '%\n')
