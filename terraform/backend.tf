terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "auroq"

    workspaces {
      name = "fin-serv"
    }
  }
}
