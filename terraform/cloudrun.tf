data "google_container_registry_image" "fin-serv" {
  name = "fin-serv"
  tag = "1.0.0"
}

resource "google_cloud_run_service" "fin-serv" {
  name     = "fin-serv"
  location = var.gcp_region

  template {
    spec {
      container_concurrency = 1
      containers {
        image = data.google_container_registry_image.fin-serv.image_url
        ports {
          container_port = 8000
        }
        resources {
          limits = {
            "cpu"    = "1000m"
            "memory" = "256Mi"
          }
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

