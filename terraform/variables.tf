variable "gcp_service_account" {
  type        = string
  description = "GCP authentication file"
}

variable "gcp_region" {
  type    = string
  default = "us-central1"
}

variable "gcp_project_id" {
  type    = string
  default = "fin-serv-299517"
}

